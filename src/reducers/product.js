const initialState = {
    query: {},
    response: {},
    fetched: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_PRODUCT":
            return action.payload;
        case "SET_PRODUCT_QUERY":
            return Object.assign({...state}, { query: action.payload, fetched: false });
        case "SET_PRODUCT_RESPONSE":
            return Object.assign({...state}, { response: action.payload, fetched: true });
        case "SET_PRODUCT_FETCH":
            return Object.assign({...state}, { fetched: action.payload });
        default:
            return state;
    }
};