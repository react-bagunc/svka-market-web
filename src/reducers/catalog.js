const initalState = {
    open: false,
    fopen: false,
    query: {},
    response: null
};

export default (state = initalState, action) => {
    switch (action.type) {
        case "TOGGLE_CATALOG_MENU":
            return Object.assign({...state}, { 
                open: (action.payload === undefined || action.payload === "toggle") ? 
                    !state.open : 
                        action.payload 
                });
        case "TOGGLE_CATALOG_FILTER":
            return Object.assign({...state}, { 
                fopen: (action.payload === undefined || action.payload === "toggle") ? 
                    !state.fopen : 
                        action.payload 
                });
        case "SET_CATALOG_LOADMORE": {

            if (!state.response.items)
                state.response.items = [];

            state.response.items = state.response.items.concat(action.payload.items);

            return Object.assign({...state}, { fetched: true });
        }
        case "SET_CATALOG_PARENT":
            return { parent: action.payload, filter: {} };
        case "SET_CATALOG_QUERY":
            return Object.assign({...initalState}, { query: action.payload, fetched: false });
        case "SET_CATALOG_RESPONSE":
            return Object.assign({...state}, { response: action.payload, fetched: true });
        case "SET_CATALOG_FETCH":
            return Object.assign({...state}, { fetched: action.payload });
        case "SET_CATALOG_ORDER":
            return Object.assign({...state}, { order: action.payload, fetched: false });
        case "SET_CATALOG_Filter": {
            
            if (action.payload.filter)
                return Object.assign({...state}, { filter: action.payload.filter, fetched: true });

            let filter = {...state.filter},
                key = action.payload.uuid,
                value = action.payload.value;
            
            if (!filter[key])
                filter[key] = [];

            let values = filter[key];
            if (action.payload.isSet) {
                if (!values.includes(value)) {
                    values.push(value);
                }
            } else {
                if (values.includes(value)) {
                    values.splice(values.indexOf(value), 1);
                    if (!filter[key].length) delete filter[key];
                }
            }

            return Object.assign({...state}, { filter, fetched: true });
        }
        default: 
            return state;
    }
};