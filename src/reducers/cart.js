const initialState = {
    open: false,
    items: [] 
};

export default (state = initialState, action) => {

    switch (action.type) {
        case "TOGGLE_CART":
            return Object.assign({...state}, { open: action.payload });
        case "ADD_TO_CART": {
            state.items = state.items || [];

            let product = action.payload,
                item = state.items.filter(i => i.uuid === product.uuid);
            
            if (item = item[0]) {
                item.count = product.count;      
            } else {
                state.items.push(product)
            }
            
            return {...state};
        }
        case "REMOVE_CART_ITEM": {
            state.items = state.items || [];

            let index = state.items.findIndex(i => i.uuid === action.payload.uuid);
            
            if (index > -1)
                state.items.splice(index, 1);

            return {...state};
        }
        case "UPDATE_CART":
            return action.payload;
        default:
            return state
    }
}