const initialState = {
    open: false,
    query: {},
    fetched: false,
    response: {}
}

export default (state = initialState, action) => {
    
    switch (action.type) {
        case "SET_QUICK_VIEW":
            return Object.assign({...state}, action.payload);
        case "SET_QUICK_VIEW_QUERY":
            return Object.assign({...state}, { query: action.payload, open: true, fetched: false } );
        case "SET_QUICK_VIEW_RESPONSE":
            return Object.assign({...state}, { response: action.payload, fetched: true } );
        case "SET_QUICK_VIEW_FETCH":
            return Object.assign({...state}, { fetched: action.payload ? action.payload : !state.fetched });
        case "TOGGLE_QUICK_VIEW":
            return Object.assign({...state}, { open: action.payload ? action.payload : !state.open });
        default:
            return state;
    }
}