const initalState = {
    open: false,
    step: "login"
};

export default (state = initalState, action) => {
    switch (action.type) {
        case "TOGGLE_AUTH":
            return Object.assign({...state}, { open: action.payload ? action.payload : !state.open });
        case "SWITCH_AUTH":
            return Object.assign({...state}, { step: action.payload });
        default:
            return state;
    }
};