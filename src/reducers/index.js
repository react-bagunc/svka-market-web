import { combineReducers } from "redux";
import getpage from "./getpage";
import catalog from "./catalog";
import cart from "./cart";
import product from "./product";
import auth from "./auth";
import quickView from "./quick-view";

const allReducers = combineReducers({
    getpage, 
    catalog,
    cart,
    auth,
    quickView,
    product
});

export default allReducers;