const initalState = {
    open: false
};

export default (state = initalState, action) => {
    switch (action.type) {
        case "OPEN_MENU":
            return { open: true };
        case "CLOSE_MENU":
            return { open: true };
        case "TOGGLE_MENU":
            return { open: !state.open };
        default:
            return state;
    }
}