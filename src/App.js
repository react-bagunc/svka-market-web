import React, { Component } from 'react';
import { Provider } from 'react-redux'; 
import { createStore }  from 'redux';
import allReducers from './reducers/index';
import AppRouter from './router';

import "./assets/css/theme.css";
import "./assets/css/modal.css";
import "./assets/css/media.css";
import { detectBrowser } from './dll';


const store = createStore(allReducers);

class App extends Component {

  componentDidMount() {
    detectBrowser();
  }

  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </div>
    );
  }
}

export default App;