import React, { Component } from 'react';
import ProductCart from '../includes/product-cart';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setQuickViewFetch, setQuickViewQuery, setQuickViewResponse, toggleQuickView } from './../../actions/quick-view';
import { window_scroll } from './../../dll';

import './style.css';

class ProductQuickView extends Component {

    constructor(props) {
        super(props);

        this.response = null;

        this.close = this.close.bind(this);
        this.preventClose = this.preventClose.bind(this);
    }

    componentDidUpdate() {
        if (!this.props.quickView.fetched) {
            this.props.setQuickViewFetch(true);

            if (this.props.quickView.query) {
                fetch('//1c.uka74.ru/apisk/hs/site/getitem', {
                    method: 'post',
                    body: JSON.stringify(this.props.quickView.query)
                }).then(response => response.json())
                    .then(response => this.props.setQuickViewResponse(response));
            }
        }
    }

    close(event) {
        event.preventDefault();

        this.props.toggleQuickView(false);
        window_scroll(true);
    }

    preventClose(event) { event.stopPropagation(); }

    render() {

        let content = "",
            cls = ["modal", "modal-quick-view"];

        if (this.props.quickView.response)
            content = <ProductCart {...Object.assign(this.props.quickView.response, { uuid: this.props.quickView.uuid })} />;

        if (this.props.quickView.open)
            cls.push("open");

        return (
            <div className={cls.join(' ')} onClick={this.close}>
                <div className="inner" onClick={this.preventClose}>
                    <a href="#close" className="close" onClick={this.close}>
                        <svg viewBox="0 0 32 32"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg>
                    </a>
                    <div className="content">
                        <section id="product-section" className="section section-product no-padding">
                            <div className="content">{content}</div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return { quickView: state.quickView };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ 
        setQuickViewFetch, 
        setQuickViewQuery, 
        setQuickViewResponse, 
        toggleQuickView 
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ProductQuickView);