import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from './home/index';
import Catalog from "./catalog";
import Account from "./account";
import Product from './product';

class Content extends Component {

    render() {

        return (
            <Switch>
                <Route path="/product">
                    <Product />
                </Route>
                <Route path="/catalog">
                    <Catalog />
                </Route>
                <Route path="/account">
                    <Account />
                </Route>
                <Route path="/">
                    <HomePage response={this.props.response} />
                </Route>
            </Switch>
        );
    }
}

export default Content;