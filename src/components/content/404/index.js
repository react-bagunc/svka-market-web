import React, { Component } from 'react';

class Page404 extends Component {

    render() {
        return (
            <div className="page box full-column">
                404 Page not found
            </div>
        );
    }
}

export default Page404;