import React, { Component } from 'react';
import CatalogBanner from './banner/index';
import Breadcrumbs from './../../includes/breadcrumbs/index';
import CatalogFilter from './filter';
import CatalogGroup from './group/index';
import CatalogProdcts from './products/index';
import { withRouter } from 'react-router-dom';

import './style.css';
import { connect } from 'react-redux';
import { setCatalogFetch, setCatalogFilter, setCatalogQuery, setCatalogResponse } from '../../../actions/catalog';
import { bindActionCreators } from 'redux';
import queryString from 'query-string';
import CatalogLoadMore from './products/loadmore';

class Catalog extends Component {

    constructor(props) {
        
        super(props);

        this.state = {};
    }

    fetch() {
        if (this.props.catalog.fetched) return;
        this.props.setCatalogFetch(true);

        if (this.props.catalog.query.parent) {
            fetch('//1c.uka74.ru/apisk/hs/site/getitems', { 
                method: 'post',
                body: JSON.stringify(this.props.catalog.query)
            }).then(response => response.json())
                .then(response => {
                    this.props.setCatalogResponse(response);
                });
            }
    }

    query() {
        let query = this.props.location.pathname.split('/'),
            queryObj = {},
            catalogQuery = this.props.catalog.query,
            stringQuery = queryString.parse(this.props.location.search);

        let parent = catalogQuery.parent || query[2],
            numpage = catalogQuery.numpage,
            attributes = [],
            order = catalogQuery.order,
            filter = catalogQuery.filter;

        if (!numpage)
            numpage = stringQuery.numpage || 1;

        if (!order)
            order = stringQuery.order;

        if (!filter)
            filter = stringQuery.filter;

        if (filter) {
            let filter = JSON.parse(stringQuery.filter);
            this.props.setCatalogFilter({ filter })
            for (let key in filter) {
                attributes.push({
                    attribute: key,
                    values: filter[key]
                });
            } 
        }
        
        queryObj = {
            parent,
            numpage,
            attributes
        };

        if (order) queryObj.order = order;

        this.props.setCatalogQuery(queryObj);

    }

    componentDidMount() {
        this.query();
    }

    componentDidUpdate() {
    }

    render() {
        this.fetch();
        
        console.log(this.props.catalog.response);

        let content = <div className="box full-column"><CatalogLoadMore isload={true} /></div>;
        if (this.props.catalog.response) {
            content = <div className="page box full-column">
                <Breadcrumbs classes="light-silver" />
                <CatalogBanner />
                <div className="flex">
                    <CatalogFilter />
                    <div className="col col-catalog">
                        <CatalogGroup />
                        <CatalogProdcts />
                    </div>
                </div>
            </div>;
        }

        return content;
    }
}

function mapStateToProps(state) {
    return { catalog: state.catalog }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({ setCatalogFetch, setCatalogFilter, setCatalogQuery, setCatalogResponse }, dispatch);
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(Catalog));