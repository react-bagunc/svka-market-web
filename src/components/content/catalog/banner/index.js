import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import './style.css';
import { connect } from 'react-redux';

class CatalogBanner extends Component {
    
    render() {

        const banner = this.props.catalog.response.banner ? 
            <LazyLoadImage src={this.props.catalog.response.banner} effect="black-and-white" /> : 
                "";

        return (
            <section id="banner-section" className={`section section-banner half-padding ${!banner ? 'hidden' : ''}`}>
				<div className="content">
                    <div className="banner">
                        {banner}
                    </div>
                </div>
			</section>
        );
    }

}

function mapStateToProps(state) {

    return { catalog: state.catalog };
}

export default connect(mapStateToProps)(CatalogBanner);