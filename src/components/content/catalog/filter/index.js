import React, { Component } from 'react';
import CatalogFilterItem from './item';
import './style.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCatalogFetch, setCatalogFilter, toggleCatalogFilter } from './../../../../actions/catalog';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import { is_mobile_mode } from '../../../../dll';
import SvgSymbol from '../../../includes/svg';

class CatalogFilter extends Component {

	constructor(props) {
		super(props);

		this.doFilter = this.doFilter.bind(this);
		this.resetFilter = this.resetFilter.bind(this);
		this.scroll = this.scroll.bind(this);
		this.toggleCatalogFilter = this.toggleCatalogFilter.bind(this);
	}

	doFilter() {
		let search = queryString.parse(this.props.location.search) || {};
		search.filter = JSON.stringify(this.props.catalog.filter);
		this.props.history.push(`/catalog/${this.props.parent}/?${queryString.stringify(search)}`);
		this.props.setCatalogFetch(false);
		this.props.toggleCatalogFilter(false);
	}
	
	resetFilter() {
		let search = queryString.parse(this.props.location.search) || {};
		if (search.filter) delete search.filter;
		
		this.props.history.push(`/catalog/${this.props.parent}/?${queryString.stringify(search)}`);
		this.props.setCatalogFilter({ filter: {} });
		this.props.setCatalogFetch(false);
		this.props.toggleCatalogFilter(false);
	}

	scroll() {
		if (!is_mobile_mode()) {
			if (window.scrollY >= this.offsetTop) {
				if (!this.refs.catalogFilter.classList.contains('fixed'))
					this.refs.catalogFilter.classList.add('fixed');
			} else if (window.scrollY < this.offsetTop) {
				if (this.refs.catalogFilter.classList.contains('fixed'))
					this.refs.catalogFilter.classList.remove('fixed');
			}
		}
	}

	toggleCatalogFilter(event) {
        event.preventDefault();

        this.props.toggleCatalogFilter();
    }

	componentDidMount() {
		this.offsetTop = this.refs.catalogFilter.offsetTop + (document.querySelector("#top-header") ? document.querySelector("#top-header").offsetHeight : 0);
		window.addEventListener('scroll', this.scroll, false);
	}

	componentDidUpdate() {
		this.offsetTop = this.refs.catalogFilter.offsetTop + (document.querySelector("#top-header") ? document.querySelector("#top-header").offsetHeight : 0);
		if (is_mobile_mode()) {
			let html = document.querySelector('html'); 
			if (this.props.catalog.fopen) {
				if (!html.classList.contains('catalog-filter-open')) {
					html.classList.add('no-scroll', 'catalog-filter-open');
				}
			} else {
				if (html.classList.contains('catalog-filter-open')) {
					html.classList.remove('no-scroll', 'catalog-filter-open');
				}
			}
		}
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.scroll, false);
	}

    render() {

        const attributes = this.props.catalog.response.attributes || [];
        return (
			<div className="col col-filter">
				<div ref="catalogFilter" id="catalog-filters" className="catalog-filters">
					<div className="content">
						<div className="topbar">
							<div className="flex v-center">
								<div className="col col-close">
									<a href="#toggle-catalog-filter" onClick={this.toggleCatalogFilter}>
										<SvgSymbol className="svg" id="svg-icon-close" />
									</a>
								</div>
								<div className="col col-caption">
									<h5 className="caption">Фильтер</h5>
								</div>
								<div className="col col-reset">
									<a href="#reset-catalog-filter" onClick={this.resetFilter}>Сбросить</a>
								</div>
							</div>
						</div>
						<form id="catalogFilterForm" action="#">
							<div className="widget filters">
								{attributes.map((attr) => <CatalogFilterItem key={attr.uuid} attr={attr} collapsed />)}
							</div>
							<div className="widget actions">
								<div className="filter">
									<div className="content">
										<button type="button" className="button expanded" onClick={this.doFilter}>Показать</button>
										<button type="button" className="button secondary expanded" onClick={this.resetFilter}>Сбросить</button>
									</div>
								</div>
							</div>
						</form> 
					</div>
				</div>
				<a href="#toggle-filter" className="toggle-filter" onClick={this.toggleCatalogFilter}>
					<SvgSymbol className="svg" id="svg-icon-filter" />
				</a>
			</div>
        );
    }

}

function mapStateToProps(state) {
	return { catalog: state.catalog }
}

function matchDispatchToProps(dsipatch) {

	return bindActionCreators({ setCatalogFetch, setCatalogFilter, toggleCatalogFilter }, dsipatch);
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(CatalogFilter));