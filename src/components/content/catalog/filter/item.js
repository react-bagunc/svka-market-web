import React, { Component } from 'react';
import InputCheckbox from '../../../includes/input/checkbox';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCatalogFilter } from './../../../../actions/catalog';

class CatalogFilterItem extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            open: props.open === true,
            ready: false
        }

        this.openContent = this.openContent.bind(this);
        this.closeContent = this.closeContent.bind(this);
        this.toggleContent = this.toggleContent.bind(this);
        this.setFilterItem = this.setFilterItem.bind(this);
    }

    openContent(e) {
        e.preventDefault();

        this.setState({ open: true });
    }

    closeContent(e) {
        e.preventDefault();

        this.setState({ open: false });
    }

    toggleContent(e) {
        e.preventDefault();

        this.setState({ open: !this.state.open });
    }

    setFilterItem(e) {

        let target = e.currentTarget;
        
        this.props.setCatalogFilter({
            uuid: this.props.attr.uuid,
            value: target.value,
            isSet: target.checked
        });
    }

    componentDidMount() {

        let wrapper,
            inner;

        if (wrapper = this.refs[this.props.attr.uuid]) {
            if (inner = wrapper.querySelector('.inner')) {
                inner.style.maxHeight = `${inner.offsetHeight}px`;
                this.setState({ ready: true });
            }
        }
    }

    render() {
        let cls = ["filter"],
            heading = this.props.attr.description || "",
            uuid = this.props.attr.uuid,
            attrs = this.props.attr.values || [];

        if (this.props.collapsed && this.state.ready) {
            if (this.state.open) cls.push("open");
            if (this.state.ready) cls.push("ready");
            if (this.props.collapsed) cls.push("is-collapsed");
        }

        const filter = this.props.catalog.filter || {};
        return (
            <div ref={uuid} className={cls.join(' ')}>
				<div className="content">
					<h5 className="filter-name"><a href="#" onClick={this.toggleContent}>{heading}</a></h5>
					<div className="inner">
                        {attrs.map((attr) => {
                            let icProps = {
                                key: attr.uuid,
                                type: "checkbox",
                                text: attr.description,
                                value: attr.uuid,
                                onChange: this.setFilterItem,
                                checked: !!(filter[this.props.attr.uuid] && filter[this.props.attr.uuid].includes(attr.uuid)),
                            }
                            return <InputCheckbox {...icProps} /> 
                        })}
                    </div>
				</div>
			</div>
        );
    }
}

function mapStateToProps(state) {

    return { catalog: state.catalog }
}

function matchDispatchToProps(dsipatch) {

	return bindActionCreators({ setCatalogFilter }, dsipatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CatalogFilterItem);