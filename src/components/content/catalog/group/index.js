import React, { Component } from 'react';
import GroupItem from './../../../includes/group-item/index';
import { connect } from 'react-redux';

class CatalogGroup extends Component {

    render() {

        const list = this.props.catalog.response.groups || [];

        return (
            <section id="top-categories-section" className="section section-top-categories categories align-center no-t-padding no-b-padding has-filter">
				<div className="content">
					<div className="grid inline-5">
                        {list.map((group) => <GroupItem key={group.uuid} {...group} />)}
                    </div>
				</div>
			</section>
        );
    }
}

function mapStateToProps(state) {

    return { catalog: state.catalog }
}

export default connect(mapStateToProps)(CatalogGroup);