import React, { Component } from 'react';
import ProductItem from './../../../includes/product/index';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCatalogOrder } from './../../../../actions/catalog';
import CatalogLoadMore from './loadmore';

class CatalogProducts extends Component {

	constructor(props) {
		super(props);

		this.loadMoreTop = 0;

		this.isLoadMore = false;

		this.orderMaps = [
			{ id: 1, label: "Наименование" },
			{ id: 2, label: "Цене, сначала недорогие" },
			{ id: 3, label: "Цене, сначала дорогие" },
			{ id: 4, label: "Производителю" },
			{ id: 5, label: "Производителю и цене" },
		];

		this.sortingCatalog = this.sortingCatalog.bind(this);

	}

	sortingCatalog(e) {
		let search = queryString.parse(this.props.location.search);
		search.order = e.currentTarget.value;
		this.props.history.push(`/catalog/${this.props.catalog.response.parent}/?${queryString.stringify(search)}`);
		this.props.setCatalogOrder(parseInt(e.currentTarget.value));
	}

    render() {
		const items = this.props.catalog.response.items || [];
		const searchQuery = queryString.parse(this.props.location.search);
		const loadMoreBlock = parseInt(this.props.catalog.query.numpage) < this.props.catalog.response.maxpage ? 
			<CatalogLoadMore numpage={this.props.catalog.response.numpage} /> : "";

		return (
            <section id="catalog-section" className="section section-catalog has-filter">
				<div className="content">
					<div className="actions-bar flex h-end v-center">
						<div className="col col-sorting">
							<div className="content">
								<button id="toggle-mobile-filter" className="button mobile-filter-button">Фильтры</button>
								<span className="caption">Сортировать по:</span>
								<select onChange={this.sortingCatalog} defaultValue={searchQuery.order}>
									{this.orderMaps.map((item) => 
										<option key={`order-catalog-${item.id}`} value={item.id}>{item.label}</option>
									)}
								</select>
							</div>
						</div>
					</div>
					<div ref="productsRoot" className="products">
                        {items.map((item) => <div className="box" key={item.uuid}><ProductItem {...item} /></div>)}
                    </div>
					{loadMoreBlock}
				</div>
			</section>
        );
    }

}

function mapStateToProps(state) {
	return { catalog: state.catalog }
}

function matchDispatchToProps(dispatch) {

	return bindActionCreators({ setCatalogOrder }, dispatch)
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(CatalogProducts));