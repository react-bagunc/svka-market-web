import React, { Component } from 'react';
import SvgSymbol from '../../../includes/svg';
import { connect } from 'react-redux';
import { setCatalogLoadMore } from '../../../../actions/catalog';
import { bindActionCreators } from 'redux';

class CatalogLoadMore extends Component {

    constructor(props) {
        super(props)

        this.ref = {};
        this.loadMoreTop = 0;

        this.query = {...this.props.catalog.query};

        this.state = { isload: this.props.isload || false };


    }

    componentDidMount() {
        this.ref = this.refs.loadMoreBlock;
        this.loadMoreTop = this.ref.offsetTop;
        
        if (this.props.catalog.response) {
            window.addEventListener('scroll', () => {
                if (
                    window.scrollY > (this.loadMoreTop - 500) && 
                    !this.state.isload &&
                    this.query.numpage < this.props.catalog.response.maxpage
                ) {
                    this.setState({ isload: true });
                    this.query.numpage = parseInt(this.query.numpage) + 1;
                    
                    fetch('//1c.uka74.ru/apisk/hs/site/getitems', { 
                        method: 'post',
                        body: JSON.stringify(this.query)
                    }).then(response => response.json())
                        .then(response => {
                            this.props.setCatalogLoadMore(response);
                            this.setState({ isload: false });
                        });
                }
            });
        }
    }

    componentDidUpdate() {
        this.loadMoreTop = this.ref.offsetTop;
	}

    render() {
        
        let cls = this.state.isload ? "loading" : "",
            show = true;

        if (this.props.catalog.response)
            if (this.query.numpage >= this.props.catalog.response.maxpage)
                show = false;

        return (
            <div ref="loadMoreBlock" id="load-more-block">
                { show ?
                <div className="load-more-block">
                    <a href="#load-more" className={cls}>
                        <SvgSymbol className="icon" id="svg-icon-load-more" />
                    </a>
                </div> : "" }
            </div>
        );
    }
}

function mapStateToProps(state) {

    return { catalog: state.catalog };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ setCatalogLoadMore }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CatalogLoadMore);