import React, { Component } from 'react';
import Slider from "react-slick";
import "./style.css";

class TopSlider extends Component {

    render() {

		const banners = this.props.banners || [];
		const settings = {
			dots: true,
			arrows: false,
			infinite: false,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'progressive'
		};

		let sliders = [];
		if (banners.length) {
			banners.map((item, idx) => {
				if (item.banner)
					sliders.push(
						<div key={`top-slider-${idx}`} className="item">
							<img src={item.banner} alt={`Banner ${idx}`} />
							<div className="inner">
								{item.name ? <h4 className="caption">{item.name}</h4> : ""}
								{item.desc ? <p className="description">Последняя модель популярных часов</p> : ""}
								<a href="#" className="button">Подробнее</a>
							</div>
						</div>
					);
			});
		}

        return (
            <div id="top-slider" className="top-slider">
				<div className="content">
					<Slider id="top-news-carousel" className="carousel carousel-top-news" {...settings}>
						{sliders}
					</Slider>
				</div>
			</div>
        );
    }
}

export default TopSlider;