import React, { Component } from 'react';
import SvgSymbol from './../../../includes/svg/index';
import "./style.css";

class Advantages extends Component {

    render() {
        return (
            <section id="advantages-section" className="section section-advantages box advantages full-column align-center">
				<div className="content">
					<h2 className="section-heading">Преимущества</h2>
                    <div className="grid inline-4">
                        <div className="box">
                            <div className="inner">
                                <div className="icon">
                                    <SvgSymbol class="icon main-plus-delivery" id="svg-icon-plus-delivery" fromDefs />
                                </div>
                                <p className="decription">Быстрая доставка до вашего дома в течение двух дней</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="inner">
                                <div className="icon">
                                    <SvgSymbol class="icon main-plus-payment" id="svg-icon-plus-payment" fromDefs />
                                </div>
                                <p className="decription">Оплачивайте удобным способом с кредитной программой</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="inner">
                                <div className="icon">
                                    <SvgSymbol class="icon main-plus-discount" id="svg-icon-plus-discount" fromDefs />
                                </div>
                                <p className="decription">Гибкая программа лояльности и накопительная система скидок</p>
                            </div>
                        </div>
                        <div className="box">
                            <div className="inner">
                                <div className="icon">
                                    <SvgSymbol class="icon main-plus-return" id="svg-icon-plus-return" fromDefs />
                                </div>
                                <p className="decription">Простой возврат товара в течение 14 дней после покупки</p>
                            </div>
                        </div>
                    </div>
				</div>
			</section>
        );
    }
}

export default Advantages;