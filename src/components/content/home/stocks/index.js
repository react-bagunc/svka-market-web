import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import "./style.css";

class Stocks extends Component {

    render() {
		const banners = this.props.banners || [];
        return (
            <section id="stocks-section" className="section section-stocks box stocks no-padding full-column">
				<div className="content">
					<div className="grid">
						{banners.map((item) => {
							let cls = ["box"];
							if (item.name === "homeContentBanner3")
								cls.push("v-2x");
							
							if (item.name === "homeContentBanner6")
								cls.push("h-2x");

							return (
								<div key={item.name} id={item.name} className={cls.join(" ")}>
									<a href="#">
										{item.banner1 ? 
											<LazyLoadImage 
												src={item.banner1}
												effect="black-and-white"
												wrapperClassName="primary-banner"
												/>
											: ""}
										{item.banner2 ? 
											<LazyLoadImage 
												src={item.banner2}
												effect="black-and-white"
												wrapperClassName="secondary-banner"
												/>
											: ""}
									</a>
								</div>
							);
						})}
					</div>
				</div>
			</section>
        );
    }
}

export default Stocks;