import React, { Component } from "react";
import Slider from "react-slick";
import "../../../includes/carousel/style.css";
import "./style.css";

class Brands extends Component {

    render() {
		const settings = {
			dots: true,
			arrows: true,
			infinite: false,
			speed: 500,
			slidesToShow: 8,
			slidesToScroll: 4,
			responsive: [
				{
					breakpoint: 997,
					settings: {
						slidesToShow: 6
					}
				},
				{
					breakpoint: 757,
					settings: {
						slidesToShow: 4
					}
				}
			]
		};

		const Items = [];
		for (let i = 0; i < 33; i++)
			Items.push(
				<div key={`brand-${i}`} className="item">
					<a href="#">
						<img src={process.env.PUBLIC_URL + `/assets/images/brands/${i}.jpg`} />
					</a>
				</div>
			);

        return (
            <section id="brands-section" className="section section-brands center-heading box full-column">
				<div className="content">
					<h2 className="section-heading">Бренды</h2>
					<Slider id="top-news-carousel" className="carousel carousel-brands arrow-square align-center" {...settings}>
						{Items}
					</Slider>
				</div>
			</section>
        );
    }
}

export default Brands;