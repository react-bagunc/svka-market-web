import React, { Component } from 'react';
import SvgSymbol from './../../../includes/svg/index';
import "./style.css";

class HomeCatalog extends Component {

    render() {
        return (
            <section id="catalog-section" className="section section-catalog box full-column">
				<div className="content">
					<div className="tabs">
						<nav>
							<ul className="menu flex h-center v-center">
								<li className="current"><a href="#recomend">Мы рекомендуем</a></li>
								<li><a href="#newest">Новинки</a></li>
								<li><a href="#hits">Хиты продаж</a></li>
								<li><a href="#discount">Скидки</a></li>
							</ul>
							<select>
								<option value="recomend">Мы рекомендуем</option>
								<option value="newest">Новинки</option>
								<option value="hits">Хиты продаж</option>
								<option value="discount">Скидки</option>
							</select>
						</nav>
					</div>
					<div className="products">
                    </div>
					<div className="load-more-block">
						<a href="#load-more">
                            <SvgSymbol class="icon" id="svg-icon-load-more" />
							<span>Показать еще</span>
						</a>
					</div>
				</div>
			</section>
        );
    }
}

export default HomeCatalog;