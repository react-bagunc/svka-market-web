import React, { Component } from 'react';
import Slider from "react-slick";
import "../../../includes/carousel/style.css";
import "./style.css";

class News extends Component {

    render() {
		const settings = {
			dots: true,
			arrows: true,
			infinite: false,
			lazyLoad: true,
			speed: 500,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1237,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 997,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 757,
					settings: {
						slidesToShow: 1
					}
				}
			]
		};

        return (
            <section id="news-section" className="section section-news box full-column white center-heading bg-full primary">
				<div className="content">
					<h2 className="section-heading">Новости</h2>
					<Slider id="top-news-carousel" className="carousel custom-nav-dots arrow-square arrow-large" {...settings}>
						<div className="item has-thumbnail">
							<a href="#">
								<div className="thumbnail">
									<img src={process.env.PUBLIC_URL + '/assets/images/sliders/news/0.jpg'} />
								</div>
								<div className="content">
									<span className="datetime">14.05.2017</span>
									<h4 className="heading">KITH х adidas adizero Prime BOOST LTD Pack</h4>
									<p className="description">
										Adidas и KITH уже не раз радовали нас качественными коллаборациями. Сейчас мы убедимся в том, 
										что и на этот раз они нас не подвели!
									</p>
								</div>
							</a>
						</div>
						<div className="item has-thumbnail">
							<a href="#">
								<div className="thumbnail">
									<img src={process.env.PUBLIC_URL + '/assets/images/sliders/news/1.jpg'} />
								</div>
								<div className="content">
									<span className="datetime">11.05.2017</span>
									<h4 className="heading">Коллекционное издание аромата Yves Saint Laurent</h4>
									<p className="description">
										Yves Saint Laurent выпустил коллекционое издание полюбившегося многим аромата Mon Paris (2016 г.). 
										Новинка называется Mon Paris Star Edition, и вышла она в рамках коллекции Star Edition 2017,...
									</p>
								</div>
							</a>
						</div>
						<div className="item">
							<a href="#">	
								<div className="content">
									<span className="datetime">10.05.2017</span>
									<h4 className="heading">Темный шоколад. 10 преимуществ.</h4>
									<p className="description">
										10 причин, почему вы должны добавить немного темного шоколада в свой ежедневный рацион, 
										и как это небольшое нововведение принесет вам огромную пользу
									</p>
								</div>
							</a>
						</div>
						<div className="item has-thumbnail">
							<a href="#">
								<div className="thumbnail">
									<img src={process.env.PUBLIC_URL + '/assets/images/sliders/news/2.jpg'} />
								</div>
								<div className="content">
									<span className="datetime">09.05.2017</span>
									<h4 className="heading">iPhone 7 Plus опередил Galaxy S8 и S8+</h4>
									<p className="description">
										Создатели AnTuTu опубликовали свежие данные о самых мощных смартфонах на рынке. Данный рейтинг, 
										отображающий ситуацию на конец апреля этого года, был составлен на основании результатов тестов,...
									</p>
								</div>
							</a>
						</div>
						<div className="item">
							<a href="#">
								<div className="content">
									<span className="datetime">08.05.2017</span>
									<h4 className="heading">Выбор первой игрушки для малыша</h4>
									<p className="description">
										Чтобы эффективно развивать органы чувств малыша, а также мышление и моторику, важно 
										позаботиться о выборе правильных, безопасных игрушек, которые также подарят 
										ему и много позитивных эмоций.
									</p>
								</div>
							</a>
						</div>
						<div className="item">
							<a href="#">
								<div className="content">
									<span className="datetime">05.05.2017</span>
									<h4 className="heading">Новые часы Dior с двойным браслетом</h4>
									<p className="description">
										Часы La D de Dior выполнены по лекалу сдержанных мужских моделей, но всегда 
										отличаются подчеркнуто женственными деталями.
									</p>
								</div>
							</a>
						</div>
						<div className="item has-thumbnail">
							<a href="#">
								<div className="thumbnail">
									<img src={process.env.PUBLIC_URL + '/assets/images/sliders/news/3.jpg'} />									
								</div>
								<div className="content">
									<span className="datetime">03.05.2017</span>
									<h4 className="heading">Lego Star Wars 2</h4>
									<p className="description">
										Каждый из нас не раз слышал и мечтал получить в подарок легендарную вселенную, 
										созданную знаменитым Джорджем Лукасом.
									</p>
								</div>
							</a>
						</div>
					</Slider>
				</div>
			</section>
        );
    }
}

export default News;