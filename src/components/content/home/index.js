import React, { Component } from "react";

import TopSlider from './top-slider/index';
import Advantages from './advantages/index';
import Stocks from './stocks/index';
import TopCategories from './top-categories/index';
import HomeCatalog from './catalog/index';
import News from './news/index';
import Instagram from './instagram/index';
import About from './about/index';
import Brands from './brands/index';

class HomePage extends Component {

    render() {
        const response = this.props.response || {};
        return (
            <div className="page box full-column">
                <TopSlider banners={response.mainbanners} />
                <Advantages />
                <Stocks banners={response.banners} />
                <TopCategories />
                <HomeCatalog />
                <News />
                <Instagram />
                <About />
                <Brands />
            </div>
        );
    }
}

export default HomePage;