import React, { Component } from 'react';
import "./style.css";

class About extends Component {

    render() {
        return (
            <section id="about-company-section" className="section section-about-company box full-column">
				<div className="content flex v-center">
					<div className="col col-image">
						<div className="thumbnail">
							<img src={process.env.PUBLIC_URL + '/assets/images/about/1.jpg'} />
						</div>
					</div>
					<div className="col col-content">
						<h2 className="section-heading">О компании</h2>
						<p className="description">
							Наша команда разрабатывает готовые маркетплейс решения для интернет-коммерции на CMS 1С-Битрикс. 
							Мы осознанно пришли в маркетплейс со своим продуктом. Нам всегда казалось, что маркетплейс магазинам 
							не хватает эстетики и простоты? и что при всем своем функционале такие интернет-магазины не очень 
							универсальны и удобны для успешных проектов. Мы глубоко убеждены, что интернет-магазин должен быть 
							не только функциональным, но и визуально располагающим к процессам покупки. Говорят, что лучший 
							дизайн - тот, которого не видно. Именно от него зависит где окажется покупатель после перехода в магазин, 
							ощутит ли он себя на огромном китайском складе или же - попадает в атмосферу красивого и уютного маркета , 
							где можно легко найти и купить нужный товар.  
						</p>
						<a href="" className="button small primary">Читать далее</a>
					</div>
				</div>
			</section>
        );
    }
}

export default About;