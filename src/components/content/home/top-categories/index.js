import React, { Component } from 'react';
import "./style.css";

class TopCategories extends Component {

    render() {
        return (
            <section id="top-categories-section" className="section section-top-categories box categories full-column align-center">
				<div className="content">
					<h2 className="section-heading">
						Популярные категории
						<a href="#" className="see-more">Смотреть все</a>
					</h2>
					<div className="grid inline-5">
						<div className="box">
							<a href="#">
								<div className="image">
									<img src={process.env.PUBLIC_URL + "/assets/images/categories/top/1.jpg"} />
								</div>
								<h3 className="heading">Детям и мамам</h3>
							</a>
						</div>
						<div className="box">
							<a href="#">
								<div className="image">
									<img src={process.env.PUBLIC_URL + "/assets/images/categories/top/2.jpg"} />
								</div>
								<h3 className="heading">Дом и сад</h3>
							</a>
						</div>
						<div className="box">
							<a href="#">
								<div className="image">
									<img src={process.env.PUBLIC_URL + "/assets/images/categories/top/3.jpg"} />
								</div>
								<h3 className="heading">Одежда, обувь, акссесуары</h3>
							</a>
						</div>
						<div className="box">
							<a href="#">
								<div className="image">
									<img src={process.env.PUBLIC_URL + "/assets/images/categories/top/4.jpg"} />
								</div>
								<h3 className="heading">Зимний спорт</h3>
							</a>
						</div>
						<div className="box">
							<a href="#">
								<div className="image">
									<img src={process.env.PUBLIC_URL + "/assets/images/categories/top/5.jpg"} />
								</div>
								<h3 className="heading">Фотоаппараты</h3>
							</a>
						</div>
					</div>
					<div className="see-more-block">
						<a href="#" className="see-more">Смотреть все</a>
					</div>
				</div>
			</section>
        );
    }
}

export default TopCategories;