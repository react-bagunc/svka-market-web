import React, { Component } from 'react';
import { IMaskInput } from 'react-imask';
import { phoneToString } from '../../../dll';

class PersonalData extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            surname: "",
            patronymic: "",
            phone: ""
        }

        this.savePersonalData = this.savePersonalData.bind(this);
    }

    savePersonalData() {

        let formData = {};
        ["name", "surname", "patronymic", "phone"].forEach(name => {
            let field = this.refs.form.querySelector(`[name="${name}"]`),
                value = field.value;

            if (name === "phone") value = phoneToString(value);

            formData[name] = value;
        });

        fetch('//1c.uka74.ru/apisk/hs/site/saveprofile', {
            method: 'post',
            body: JSON.stringify(Object.assign({...formData}, { 
                access_token: localStorage.getItem('svka_access_token') 
            }))
        }).then(response => response.json())
            .then(response => {
                if (response.status) {
                    this.setState(formData)
                }
            });
        
    }

    fetch() {
        fetch('//1c.uka74.ru/apisk/hs/site/getprofile', {
            method: 'post',
            body: JSON.stringify({
                access_token: localStorage.getItem('svka_access_token')
            })
        }).then(response => response.json())
            .then(response => {
                if (response.status)
                    this.setState(response);
            });
    }
    
    componentDidMount() {
        this.fetch();
    }

    render() {

        return (
            <div id="personal-data-block" className="block personal-data">
                <div className="form">
                    <form ref="form" action="#">
                        <h5 className="caption">Изменить личные данные</h5>
                        <label className="field">
                            <input name="name" type="text" placeholder="Имя" defaultValue={this.state.name} autoComplete="none" />
                        </label>
                        <label className="field">
                            <input name="surname" type="text" placeholder="Фамилия" defaultValue={this.state.surname} autoComplete="none" />
                        </label>
                        <label className="field">
                            <input name="patronymic" type="text" placeholder="Отчество" defaultValue={this.state.patronymic} autoComplete="none" />
                        </label>
                        <label className="field">
                            <IMaskInput name="phone" type="text" mask="+{7} (000) 000-00-00" lazy={false} value={this.state.phone} autoComplete="none" />
                        </label>
                        <button type="button" className="button small expanded" onClick={this.savePersonalData}>Сохранить</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default PersonalData;
