import React, { Component } from 'react';
import OrdersHistory from './orders-history';
import PersonalData from './personal-data';
import CurrentOrders from './current-orders';
import { Link } from 'react-router-dom';

import './style.css';

class Account extends Component {

    constructor(props) {
        super(props);

        this.tabsMap = {
            personalData: { label: "Личные данные", component: PersonalData },
            currentOrders: { label: "Текущие заказы", component: CurrentOrders },
            ordersHistory: { label: "История заказов", component: OrdersHistory }
        };

        this.state = {
            tab: "personalData"
        };

        this.switchTab = this.switchTab.bind(this);
    }

    switchTab(event, tab) {
        event.preventDefault();

        this.setState({ tab })
    }

    componentDidMount() {
        
        if (!localStorage.getItem("svka_access_token")) {
            fetch('//1c.uka74.ru/apisk/hs/site/refreshtoken', { 
                method: 'post',
                body: JSON.stringify({
                    refresh_token: localStorage.getItem('svka_refresh_token')
                })
            }).then(response => response.json())
                .then(response => {
                    if (response.status)
                        localStorage.setItem("svka_access_token", response.access_token);
                });
        }
    }

    render() {

        return (
            <div className="page box full-column">
                <section id="page-info-section" className="section section-page-info box full-column no-padding">
                    <div className="content">
                        <div className="breadcrumbs">
                            <nav className="menu">
                                <ul className="inline">
                                    <li><Link to="/">Главная</Link></li>
                                    <li><span>Личный кабинет</span></li>
                                </ul>
                            </nav>
                        </div>
                        <h1 className="section-heading">Личный кабинет</h1>
                    </div>
                </section>
                <div className="my-account-flex flex">
                    <div className="col col-menu">
                        <div id="my-account-menu" className="my-account-menu">
                            <nav className="menu">
                                <ul>
                                    {Object.keys(this.tabsMap).map((tab) => 
                                        <li key={`tab-item-${tab}`} className={tab === this.state.tab ? "current" : ""}>
                                            <a href="#" onClick={(e) => this.switchTab(e, tab)}>{this.tabsMap[tab].label}</a>
                                        </li>
                                    )}
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div className="col col-content">
                        <section id="my-account-section" className="section section-my-account no-padding">
                            <div className="content">
                                <a href="#" className="logout button secondary tiny">Выйти из аккаунта</a>
                                {React.createElement(this.tabsMap[this.state.tab].component)}
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}

export default Account;