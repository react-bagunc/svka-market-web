import React, { Component } from 'react';
import SvgSymbol from '../../includes/svg';

class CurrentOrders extends Component {

    render() {

        return (
            <div id="current-orders-block" className="block current-orders orders-block">
                <div className="order">
                    <div className="topbar">
                        <h4 className="heading">
                            Заказ №23 от 11.10.2019, 2 товара
                            <span className="status warning">Принят, ожидается оплата</span>
                        </h4>
                    </div>
                    <div className="payment info-block flex v-center">
                        <div className="col-data">
                            <div className="order">
                                Счет №23/1 от 11.10.2019, Наличные курьеру
                                <span className="label alert">НЕ ОПЛАЧЕНО</span>
                            </div>
                            <div className="total">
                                Сумма к оплате по счету:
                                <span className="price">4 769 <span className="currency rub">Р</span></span>
                            </div>
                            <a href="#">Изменить способ оплаты</a>
                        </div>
                        <div className="col-action">
                            <button className="button small">Оплатить</button>
                        </div>
                    </div>
                    <div className="shipment info-block">
                        <div className="caption">ДОСТАВКА</div>
                        <div className="order">
                            Отгрузка №23/2, стоимость доставки <span className="price">500 <span className="currnecy rub">Р</span></span>
                            <span className="label alert">Не отгружено</span>
                        </div>
                        <div className="status">
                            Статус отгрузки: <span>Ожидает обработки</span>
                        </div>
                        <div className="service">
                            Служба доставки: <span>Доставка курьером</span>
                        </div>
                    </div>
                    <div className="bottombar flex">
                        <div className="col-more">
                            <a href="#" className="more-link">Подробнее о заказе</a>
                        </div>
                        <div className="col-actions">
                            <a href="#">
                                <SvgSymbol className="icon repeat" id="#svg-icon-repeat" />
                                <span className="text">Повторить заказ</span>
                            </a>
                            <a href="#">
                                <SvgSymbol className="icon repeat" id="#svg-icon-cross" />
                                <span className="text">Отменить заказ</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CurrentOrders;
