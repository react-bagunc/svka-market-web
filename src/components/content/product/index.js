import React, { Component } from 'react';
import ProductCart from './../../includes/product-cart/index';
import TabSwitcher from './../../includes/tab-switcher/index';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { setProductQuery, setProductResponse, setProductFetch } from './../../../actions/product';
import { connect } from 'react-redux';

class Product extends Component {

    constructor(props) {
        super(props);

        this.uuid = this.props.product.query.uuid;
    }

    componentDidMount() {
        let url = this.props.location.pathname.split('/');
        this.uuid = url[2];

        this.props.setProductQuery({
            uuid: this.uuid
        });
    }

    componentDidUpdate() {
        if (!this.props.product.fetched) {
            this.props.setProductFetch(true);
            fetch('//1c.uka74.ru/apisk/hs/site/getitem', {
                method: "post",
                body: JSON.stringify({
                    uuid: this.uuid
                })
            }).then(response => response.json())
                .then(response => this.props.setProductResponse(response));
        }
    }

    render() {
        return (
            <div className="page box full-column">
                <section id="product-section" className="section section-product has-additional-section bg-full light-silver no-t-padding">
                    <div className="content">
                        <ProductCart {...this.props.product.response} />
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return { product: state.product };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ setProductResponse, setProductQuery, setProductFetch }, dispatch)
}

export default withRouter(connect(mapStateToProps, matchDispatchToProps)(Product));