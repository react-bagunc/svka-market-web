import React, { Component } from 'react';

import { Link } from "react-router-dom";

import SvgSymbol from './../../includes/svg/index';
import './style.css';
import { bindActionCreators } from 'redux';
import { toggleCart } from './../../../actions/cart';
import { connect } from 'react-redux';
import { toggleCatalogMenu } from './../../../actions/catalog';
import { toggleAuth } from './../../../actions/auth';

class TopHeader extends Component {

	constructor(props) {
		super(props);

		this.toggleMenu = this.toggleMenu.bind(this);
		this.toggleCart = this.toggleCart.bind(this);
		this.toggleAuth = this.toggleAuth.bind(this);
	}

	toggleMenu(event) {
		event.preventDefault();

		let toggle = !document.querySelector('html').classList.contains('slideout-open');
		this.props.toggleCatalogMenu(toggle);
	}

	toggleCart(event) {
		event.preventDefault();

		this.props.toggleCart(true);
	}
	
	toggleAuth(event) {
		event.preventDefault();

		this.props.toggleAuth(true);
	}

    render() {

		const cartItemsLength = this.props.cart.items.length || 0;

        return (
            <div id="top-header" className="box top-header">
				<div className="content flex v-center">
					<div className="col col-logo">
						<a id="toggle-mobile-menu" className="toggle-mobile-menu" href="#toggle-mobile-menu" onClick={this.toggleMenu}>
                            <SvgSymbol class="icon" id="svg-icon-m-toggle" />
                        </a>
						<Link to="/" className="logo">
							<SvgSymbol class="icon" id="svg-icon-logo" fromDefs />
						</Link>
					</div>
					<div className="col col-primary-menu">
						<nav id="primary-menu" className="menu inline primary-menu">
							<ul className="flex">
								<li><a href="#">О компании</a></li>
								<li><a href="#">Доставка</a></li>
								<li><a href="#">Оплата</a></li>
								<li><a href="#">Услуги</a></li>
								<li><a href="#">Акции</a></li>
							</ul>
						</nav>
					</div>
					<div className="col col-user">
						<div className="flex v-center h-end">
							<div className="col col-store-icons">
								<ul className="store-icons flex v-center">
									<li className="search">
										<a href="#toggle-header-search"><SvgSymbol class="icon" id="svg-icon-search" /></a>
									</li>
									<li className="like">
										<a 	href="#toggle-liked-modal" 
											className="modal-opener">
											<span className="relative">
                                                <SvgSymbol class="icon" id="svg-icon-liked" />
												<span className="count" title="0 товар"></span>
											</span>
										</a>
									</li>
									<li className="cart">
										<a href="./cart.html" className="open-cart" onClick={this.toggleCart}>
											<span className="relative">
                                                <SvgSymbol class="icon" id="svg-icon-cart" />
												<span className="count" title="0 товар">{cartItemsLength ? cartItemsLength : ""}</span>
											</span>
										</a>
									</li>
								</ul>
							</div>
							<div className="col-auth auth flex h-end">
								<div className="col">
									<a  href="#login-modal" 
										className="modal-opener"
										onClick={this.toggleAuth}>
                                        <SvgSymbol class="icon" id="svg-icon-profile" />
										Войти
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        );
    }
}

function matchDispatchToProps(dispatch) {

	return bindActionCreators({ toggleCart, toggleCatalogMenu, toggleAuth }, dispatch)
}

function mapStateToProps(state) {

	return { cart: state.cart }
}

export default connect(mapStateToProps, matchDispatchToProps)(TopHeader);