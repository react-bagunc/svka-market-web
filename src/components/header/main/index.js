import React, { Component } from 'react';
import CatalogMenu from '../catalog-menu';
import SvgSymbol from './../../includes/svg/index';
import './style.css';
import HeaderCart from './cart';
import { is_mobile_mode } from '../../../dll';

class MainHeader extends Component {

	componentDidMount() {
		this.offsetTop = this.refs.mainHeader.offsetTop;
		let body = document.body;
		window.addEventListener('scroll', () => {
			if (!is_mobile_mode()) {
				if (window.scrollY >= this.offsetTop) {
					if (!body.classList.contains('fixed-header')) {
						body.classList.add('fixed-header');
						this.refs.mainHeader.classList.add('fixed-content');
					}
				} else if (window.scrollY < this.offsetTop) {
					if (body.classList.contains('fixed-header')) {
						body.classList.remove('fixed-header');
						this.refs.mainHeader.classList.remove('fixed-content');
					}
				}
			}
		});
	}

    render() {

        return (
            <header ref="mainHeader" id="header" className="box header full-border-top full-border-bottom">
				<div className="content flex v-center">
					<div className="col-catalog-menu">
						<CatalogMenu toggleMenu={this.props.toggleMenu} response={this.props.response} />
					</div>
					<div className="col col-search">
						<form action="#" method="post">
							<div className="relative">
								<button name="search">
                                    <SvgSymbol class="icon" id="svg-icon-search" />
								</button>
								<input type="search" name="search" placeholder="Поиск по каталогу" autoComplete="false" />
							</div>
							<div id="search-autocomplete" className="search-autocomplete"></div>
						</form>
					</div>
					<div className="col col-store-data">
						<div className="flex v-center">
							<div className="col col-callback">
								<div className="content flex nowrap grow-0 v-center">
									<div className="col col-icon">
                                        <SvgSymbol class="icon" id="svg-icon-phone" />
									</div>
									<div className="col col-numbers">
										<a href="tel:88008008080">8 (999) 800-80-80</a>
										<a href="#toggle-callback-modal">Заказать обратный звонок</a>
									</div>
								</div>
							</div>
							<div className="col col-cart">
								<HeaderCart />
							</div>
						</div>
					</div>
				</div>
			</header>
        );
    }
}

export default MainHeader;