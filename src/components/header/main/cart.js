import React, { Component } from 'react';
import { totalCount, TotalPriceFormated } from './../../cart/index';
import SvgSymbol from './../../includes/svg/index';
import { bindActionCreators } from 'redux';
import { toggleCart } from './../../../actions/cart';
import { connect } from 'react-redux';

class HeaderCart extends Component {

    render() {
        return (
            <a href="./cart.html" className="open-cart flex h-start v-center" onClick={(e) => {
                    e.preventDefault();
                    this.props.toggleCart(true);
                }}>
                <div className="col col-icon relative">
                    <SvgSymbol class="icon" id="svg-icon-cart" />
                </div>
                <div className="col col-content">
                    <label>Корзина</label>
                    <span className="desc">
                        {`${this.props.cart.items.length} товар`}
                        {[` на `, <TotalPriceFormated key="headerCartTotalPrice" />]}
                    </span>
                </div>
            </a>
        );
    }

}

function mapStateToProps(state) {

    return { cart: state.cart }
}

function matchDispatchToProps(dispatch) {

	return bindActionCreators({ toggleCart }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(HeaderCart);