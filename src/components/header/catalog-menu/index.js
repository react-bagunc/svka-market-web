import React, { Component } from 'react';
import SvgSymbol from '../../includes/svg';
import "./style.css";
import { bindActionCreators } from 'redux';
import { toggleCatalogMenu } from './../../../actions/catalog';
import { connect } from 'react-redux';

class CatalogMenu extends Component {

    constructor(props) {
        super(props);

        this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu(event) {
        event.preventDefault();
        this.props.toggleCatalogMenu(true);
    }

    render() {
        return (
            <nav id="catalog-main-menu" className="menu relative catalog-main-menu e-snipet" data-control="CatalogHeaderMenu">
                <a href="#toggle-catalog-menu" onClick={this.toggleMenu} className="relative">
                    <SvgSymbol id="svg-icon-m-toggle" class="icon" />
                    Каталог товаров
                </a>
            </nav>
        );
    }

}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ toggleCatalogMenu }, dispatch)
}

export default connect(null, matchDispatchToProps)(CatalogMenu);