import React, { Component } from 'react';
import SvgSymbol from './../includes/svg/index';
import MobileCatalogMenu from './catalog-menu';
import "./style.css";
import { bindActionCreators } from 'redux';
import { toggleCatalogMenu } from './../../actions/catalog';
import { connect } from 'react-redux';
import { is_mobile_mode, window_scroll } from '../../dll';
import Slideout from 'slideout';
import { toggleAuth } from './../../actions/auth';


class MobileMenu extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            open: false
        };

        this.SliderMenu = undefined;

        this.close = this.close.bind(this);
        this.preventParentClick = this.preventParentClick.bind(this);
        this.toggleAuth = this.toggleAuth.bind(this);
    }

    close(event) {
        event.preventDefault();

        this.props.toggleCatalogMenu(false);
    }

    preventParentClick(event) {
        event.stopPropagation();
    }

    toggleAuth(event) {
        event.preventDefault();

        this.props.toggleAuth(true);
    }

    componentDidMount() {
        this.SliderMenu = new Slideout({
            panel: document.getElementById('wrapper'),
            menu: document.getElementById('mobile-menu'),
            padding: 260,
            tolerance: 70
        });
    }

    componentDidUpdate() {
        if (is_mobile_mode()) {
            if (this.SliderMenu) {
                if (this.props.catalogMenu.open)
                    this.SliderMenu.open();
                else
                    this.SliderMenu.close();
            }
        } else {
            window_scroll(!this.props.catalogMenu.open);
            let menu = this.refs.mobileMenu;
            if (!this.props.catalogMenu.open) {
                menu.classList.remove('show');
                setTimeout(() => menu.classList.remove('open'), 300);
            } else {
                menu.classList.add('open');
                setTimeout(() => menu.classList.add('show'), 300);
            }
        }
    }

    render() {
        return (
            <nav ref="mobileMenu" id="mobile-menu" className="mobile-menu" onClick={this.close}>
                <div className="m-menu-wrapper" onClick={this.preventParentClick}>
                    <div className="m-menu-secondary">
                        <a className="profile modal-opener" 
                            href="#login-modal"
                            onClick={this.toggleAuth}>
                            <SvgSymbol classses="icon" id="svg-icon-profile" />
                            <span className="text">Личный кабинет</span>
                        </a>
                        <a href="#" className="close-menu" onClick={this.close}>
                            <SvgSymbol id="svg-icon-close" />
                        </a>
                    </div>
                    <MobileCatalogMenu response={this.props.response} />
                    <div className="m-menu-primary">
                        <ul id="footer-mobile-menu" className="menu footer-mobile-menu">
                            <li className="has-submenu">
                                <a href="#">Компания</a>
                                <ul className="submenu">
                                    <li><a href="#">О компании</a></li>
                                    <li><a href="#">Новости</a></li>
                                    <li><a href="#">Акции</a></li>
                                    <li><a href="#">Контакты</a></li>
                                </ul>
                            </li>
                            <li className="has-submenu">
                                <a href="#">Интернет-магазин</a>
                                <ul className="submenu">
                                    <li><a href="#">Оплата заказа</a></li>
                                    <li><a href="#">Доставка товара</a></li>
                                    <li><a href="#">Возврат и обмен</a></li>
                                </ul>
                            </li>
                            <li className="has-submenu">
                                <a href="#">Информация</a>
                                <ul className="submenu">
                                    <li><a href="#">Поставщикам</a></li>
                                    <li><a href="#">Гарантия</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div className="m-menu-search">
                            <form action="#">
                                <input type="text" placeholder="Поиск по каталогу" />
                                <button><SvgSymbol classses="icon" id="svg-icon-search" /></button>
                            </form>
                        </div>
                        <div className="m-menu-footer">
                            <p>Контактная информация</p>
                            <p>
                                <a href="tel:88008008080">8 (999) 800-80-80</a>
                                <a href="tel:88008008080">8 (880) 800-80-80</a>
                            </p>
                            <p>
                                <a href="mailto:sale@bitlate.ru">sale@bitlate.ru</a>
                            </p>
                            <ul className="menu social-mobile-menu">
                                <li>
                                    <a href="http://facebook.com" target="_blank">
                                        <SvgSymbol classses="icon icon-social-facebook" id="svg-icon-social-facebook" />
                                        <span className="text">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://vk.com" target="_blank">
                                        <SvgSymbol classses="icon icon-social-vk" id="svg-icon-social-vk" />
                                        <span className="text">Вконтакте</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://ok.ru" target="_blank">
                                        <SvgSymbol classses="icon icon-social-ok" id="svg-icon-social-ok" />
                                        <span className="text">Одноклассники</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://twitter.com" target="_blank">
                                        <SvgSymbol classses="icon icon-social-twitter" id="svg-icon-social-twitter" />
                                        <span className="text">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://google.com" target="_blank">
                                        <SvgSymbol classses="icon icon-social-google" id="svg-icon-social-google" />
                                        <span className="text">Google +</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <SvgSymbol classses="icon icon-social-instagram" id="svg-icon-social-instagram" />
                                        <span className="text">Instagram</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {

    return { catalogMenu: state.catalog }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ toggleCatalogMenu, toggleAuth }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MobileMenu);