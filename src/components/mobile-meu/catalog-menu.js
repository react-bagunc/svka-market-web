import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCatalogParent, setCatalogQuery } from '../../actions/catalog';
import { withRouter } from 'react-router-dom';
import { is_mobile_mode } from '../../dll';

class MobileCatalogMenu extends Component {

    constructor(props) {
        super(props);

        this.openMobileSubmenu = this.openMobileSubmenu.bind(this);
        this.closeMobileSubmenu = this.closeMobileSubmenu.bind(this);
    }

    openMobileSubmenu(event, item) {
        let parent = event.currentTarget.parentElement;
        if (is_mobile_mode() && parent.classList.contains('has-submenu')) {
            parent.classList.add('open');
            setTimeout(() => parent.classList.add('show'), 1);
        } else {
            this.props.setCatalogQuery({
                parent: item.uuid,
                numpage: 1,
                attributes: []
            });
            this.props.history.push(`/catalog/${item.uuid}/`);
        }
    }
    
    closeMobileSubmenu(event) {
        event.preventDefault();

        if (!is_mobile_mode()) return;
        
        let parent = event.currentTarget.closest('.has-submenu.open');
        parent.classList.remove('show');
        setTimeout(() => {
			parent.classList.remove('open');
		}, 250)
    }

    renderSubmenu(list, item) {

        return (
            <div className="submenu-container">
                <ul className="submenu">
                    <li className="back"><a href="#" onClick={this.closeMobileSubmenu}>Вернуться назад</a></li>
                    <li className="parent"><a href="#" onClick={(e) => {e.preventDefault(); this.openMobileSubmenu(e, item)}}>{item.description}</a></li>
                    {list.map((listItem) => this.renderItem(listItem))}
                </ul>
            </div>
        );
    }

    renderItem(item) {
        let logoUrl = process.env.PUBLIC_URL + '/assets/images/placeholder.png',
            itemsCls = ["has-icon"],
            sublist = item.childnodes || [];
        
        if (item.logo)
            logoUrl = item.logo;

        if (sublist.length)
            itemsCls.push("has-submenu");

        return (
            <li key={item.uuid} className={itemsCls.join(" ")}>
                <a href="#" onClick={(e) => {e.preventDefault(); this.openMobileSubmenu(e, item)}}>
                    <span className="icon"><img src={logoUrl} /></span>
                    {item.description}
                </a>
                {sublist.length ? this.renderSubmenu(sublist, item) : ""}
            </li>
        );
    }

    render() {
        let list = this.props.response || [];
        return (
            <div id="catalog-mobile-menu" className="m-menu-catalog">
                <ul className="menu catalog-mobile-menu">
                    {list.map((item, idx) => this.renderItem(item, idx))}
                </ul>
            </div>
        );
    }
}

function mathcDispatchToProps(dispatch) {

    return bindActionCreators({ setCatalogParent, setCatalogQuery }, dispatch);
}

export default withRouter(connect(null, mathcDispatchToProps)(MobileCatalogMenu));