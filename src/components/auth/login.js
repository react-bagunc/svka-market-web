import React, { Component } from 'react';
import { IMaskInput } from 'react-imask';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { toggleAuth } from './../../actions/auth';
import { connect } from 'react-redux';

class AuthLogin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            auth: 'addUser', // addUser -> confirmCode -> SignIn
            timer: 60,
            errors: {}
        };
        
        this.action = this.action.bind(this);
        this.addUser = this.addUser.bind(this);
        this.sendCode = this.sendCode.bind(this);
        this.confirmCode = this.confirmCode.bind(this);
    }

    action() {

        this.props.history.push('/account/');
    }

    addUser(value) {
        if (value.length === 11) {
            fetch('//1c.uka74.ru/apisk/hs/site/adduser', {
                method: 'post',
                body: JSON.stringify({
                    login: value
                })
            }).then(response => response.json())
                .then(response => {
                    if (response.status) {
                        localStorage.setItem('svka_access_token', response.access_token);
                        localStorage.setItem('svka_refresh_token', response.refresh_token);
                        this.sendCode();
                    } else {
                        this.setState({ errors: Object.assign({...this.state.errors}, { phone: response.errors }) });
                    }
                });
        }
    }

    sendCode(event) {
        if (event) event.preventDefault();

        let access_token;
        if (access_token = localStorage.getItem('svka_access_token')) {
            fetch('//1c.uka74.ru/apisk/hs/site/sendcode', {
                method: 'post',
                body: JSON.stringify({
                    access_token
                })
            }).then(response => response.json())
                .then(response => {
                    if (response.status) {
                        this.setState({ auth: 'confirmCode', timer: 60, errors: {} });
                        this.startTimer();
                    } else {
                        this.setState({ errors: Object.assign({...this.state.errors}, { phone: response.errors }) });
                    }
                });
        }
    }

    startTimer() {
        let interval = setInterval(() => {
            let timer = this.state.timer - 1;
            this.setState({ timer });
            if (timer <= 0)
                clearInterval(interval);
        }, 1000);
    }

    confirmCode(code) {
        if (code.length === 4) {
            let access_token;
            if (access_token = localStorage.getItem('svka_access_token')) {
                fetch('//1c.uka74.ru/apisk/hs/site/confirmcode', {
                    method: 'post',
                    body: JSON.stringify({
                        access_token,
                        code
                    })
                }).then(response => response.json())
                    .then(response => {
                        console.log(response);
                        if (response.status) {
                            this.props.toggleAuth(false);
                            this.props.history.push('/account/');
                            this.setState({ errors: {} });
                        } else {
                            this.setState({ errors: Object.assign({...this.state.errors}, { code: response.errors }) });
                        }
                    });
            }
        }
    }

    errosList(key) {
        let errors;
        if (errors = this.state.errors[key]) {
            return (
                <span className="errors">
                    {errors.map((error) => 
                        <span key={`auth-phone-error-${error.code}`} className="error">{error.description}</span>
                    )}  
                </span>
            );
        } 
    }

    render() {

        return (
            <div className="flex v-center">
                <div className="col col-banner">
                    <img src={process.env.PUBLIC_URL + "/assets/images/login-cover.jpg"} />
                </div>
                <div className="col col-form">
                    <form action="#">
                        <label className={this.state.errors.phone ? "invalid" : ""}>
                            <span className="text">Телефон для связи</span>
                            <IMaskInput ref="phoneNumber"
                                        unmask={true}
                                        type="text" 
                                        mask="+{7} (000) 000-00-00" 
                                        lazy={false}
                                        onAccept={this.addUser}    
                                    />
                            {this.errosList("phone")}
                        </label>
                        {this.state.auth === "confirmCode" ? 
                            <div className="code">
                                <div className="flex col-2 v-center">
                                    <div className="col col-code">
                                        <label className={this.state.errors.code ? "invalid" : ""}>
                                            <span className="text">Код из СМС</span>
                                            <IMaskInput ref="phoneNumber"
                                                    unmask={true}
                                                    type="text" 
                                                    mask="0-0-0-0" 
                                                    lazy={true}
                                                    onAccept={this.confirmCode}    
                                                />
                                        </label>
                                    </div>
                                    <div className="col col-resend">
                                        {this.state.timer === 0 ?
                                            <a href="#resend-code" onClick={this.sendCode}>Отправить повторно</a> :
                                            <span>Отправить еще через <span ref="sec" className="sec">{this.state.timer}</span> сек.</span>}
                                    </div>
                                </div>
                                {this.errosList("code")}
                                <p className="hint">Код подтверждения отправлен на телефон</p>
                            </div> : ""}

                        {this.state.auth === "addUser" ?
                            <p className="privacy-policy">
                                Указывая номер телефона, вы принимаете условия
                                <Link to="/">пользовательского соглашения</Link>
                            </p> : ""}
                    </form>
                </div>
            </div>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ toggleAuth }, dispatch);
}

export default withRouter(connect(null, matchDispatchToProps)(AuthLogin));