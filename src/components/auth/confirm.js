import React, { Component } from 'react';
import SvgSymbol from './../includes/svg/index';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { switchAuth } from '../../actions/auth';

class AuthConfirm extends Component {

    render() {

        return (
            <form action="#" data-form="confirm">
                <div className="caption">Подтверждение <span className="type"></span></div>
                <div className="clock-block">
                    Вам отправлен SMS-пароль
                    <span className="clock">
                        <span className="icon">
                            <SvgSymbol class="svg" id="svg-icon-clcok" />
                        </span> 
                        <span className="min">5</span> мин <span className="sec">00</span> сек
                    </span>
                </div>
                <fieldset className="fields phone">
                    <label data-type="confirmcode" data-code="login">
                        <input type="text" id="regConfirm" placeholder="SMS-пароль" />
                        <button className="button small">Подтвердить</button>
                        <button type="button" id="confirmResend" className="button small secondary">Отпровить повторно</button>
                    </label>
                </fieldset>
            </form>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ switchAuth }, dispatch)
}

export default connect(null, matchDispatchToProps)(AuthConfirm);