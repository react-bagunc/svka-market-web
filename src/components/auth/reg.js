import React, { Component } from 'react';
import { IMaskInput } from 'react-imask';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { switchAuth } from '../../actions/auth';

class AuthReg extends Component {

    render() {

        return (
            <form action="#" data-form="registration">
                <fieldset className="fields phone">
                    <label data-type="phone" data-code="login">
                        <IMaskInput type="text" mask="+{7} (000) 000-00-00" lazy={false} />
                    </label>
                    <label className="choose policy">
                        <input type="checkbox" hidden />
                        <span className="elem"></span>
                        <span className="text">
                            Заполняя форму, Вы даете соглацие на обработку 
                            ваших персональных данных на следыющих <a href="#">условиях</a>
                        </span>
                    </label>
                    <button className="button expanded" onClick={() => this.props.switchAuth('confirm')}>Зарегистрироваться</button>
                </fieldset>
            </form>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ switchAuth }, dispatch);
}

export default connect(null, matchDispatchToProps)(AuthReg);