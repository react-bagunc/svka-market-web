import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAuth, switchAuth } from './../../actions/auth';
import AuthReg from './reg';
import AuthLogin from './login';
import AuthConfirm from './confirm';
import './style.css';
import { window_scroll } from './../../dll';

class Auth extends Component {

    constructor(props) {
        super(props);

        this.formsMap = {
            login: AuthLogin,
            reg: AuthReg,
            confirm: AuthConfirm   
        };

        this.switchAuth = this.switchAuth.bind(this);
        this.close = this.close.bind(this);
        this.preventParentClick = this.preventParentClick.bind(this);

    }

    close(event) {
        event.preventDefault();

        this.props.toggleAuth(false);
    }

    preventParentClick(event) {
        event.stopPropagation();
    }

    switchAuth(event) {
        event.preventDefault();

        this.props.switchAuth(event.currentTarget.dataset.form);
    }

    componentDidUpdate() {
        if (this.props.auth.open) {
            if (!this.refs.loginModal.classList.contains('open')) {
                window_scroll(false);
                this.refs.loginModal.classList.add('open');
            }
        } else {
            if (this.refs.loginModal.classList.contains('open')) {
                window_scroll(true);
                this.refs.loginModal.classList.remove('open');
            }
        }

    }

    render() {

        return (
            <div ref="loginModal" id="login-modal" className="modal modal-login" onClick={this.close}>
                <div className="inner" onClick={this.preventParentClick}>
                    <a href="#close" className="close" onClick={this.close}>
                        <svg viewBox="0 0 32 32"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg>
                    </a>
                    <div className="content">
                        <AuthLogin />
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return { auth: state.auth };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ toggleAuth, switchAuth }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Auth);