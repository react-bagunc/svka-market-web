import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import InputCounter from './../includes/input-counter/index';
import { bindActionCreators } from 'redux';
import { addToCart, removeCartItem } from './../../actions/cart';
import { connect } from 'react-redux';
import SvgSymbol from '../includes/svg';

class CartItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            image: process.env.PUBLIC_URL + '/assets/images/placeholder.png',
            name: "",
            price: 0
        }

        this.counterUpdate = this.counterUpdate.bind(this);
        this.remove = this.remove.bind(this);
    }

    counterUpdate(ic) {
        let value = ic.value < 0 ? 0 : ic.value,
            instance = {...this.props};

        instance.count = value;
        this.props.addToCart(instance);
    }

    remove(event) {
        event.preventDefault();
        this.props.removeCartItem({ uuid: this.props.uuid });
    }

    render() {

        let cls = ["product"];
        if (!this.props.count) cls.push("disabled");

        const icSettings = {
            value: this.props.count,
            afterUpdate: this.counterUpdate
        };

        return (
            <div className={cls.join(' ')}>
                <div className="flex">
                    <div className="col col-thumbnail">
                        <LazyLoadImage 
                            src={this.props.preview}
                            effect="black-and-white"
                            />
                    </div>
                    <div className="col col-info">
                        <h4 className="name">{this.props.name}</h4>
                    </div>
                    <div className="col col-count counter-active">
                        <InputCounter {...icSettings} />
                    </div>
                    <div className="col col-remove">
                        <a href="#remove" onClick={this.remove}>
                            <SvgSymbol class="svg circle" id="svg-icon-circle" />
                            <SvgSymbol class="svg remove" id="svg-icon-remove" />
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ addToCart, removeCartItem }, dispatch);
}

export default connect(null, matchDispatchToProps)(CartItem);
