import React, { Component } from 'react';
import SvgSymbol from './../includes/svg/index';

import './style.css';
import { bindActionCreators } from 'redux';
import { toggleCart, updateCart } from './../../actions/cart';
import { connect } from 'react-redux';
import { window_scroll } from './../../dll';
import CartItem from './item';
import Price from '../includes/price';

class Cart extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            open: false,
        }

        this.close = this.close.bind(this);
        this.innerPrevent = this.innerPrevent.bind(this);
    }

    close(event) {
        event.preventDefault();

        this.props.toggleCart(false);
    }

    innerPrevent(event) {
        event.stopPropagation();
    }

    componentDidMount() {
        let localCart;
        if (localCart = localStorage.getItem('svkaCart')) {
            localCart = JSON.parse(localCart);
        } else {
            localCart = { items: [] };
        }

        this.props.updateCart(localCart);
    }

    componentDidUpdate() {
        localStorage.setItem('svkaCart', JSON.stringify(this.props.cart || {}));
        if (this.props.cart.open) {
            if (!this.refs.cartModal.classList.contains('open')) {
                window_scroll(false);
                this.refs.cartModal.classList.remove('close');
                this.refs.cartModal.classList.add('open');
                setTimeout(() => {
                    this.refs.cartModal.classList.add('animate');
                }, 300);
            }
        } else {
            if (this.refs.cartModal.classList.contains('open')) {
                this.refs.cartModal.classList.remove('animate');
                setTimeout(() => {
                    this.refs.cartModal.classList.remove('open');
                    this.refs.cartModal.classList.add('close');
                    window_scroll(true);
                }, 300);
            }
        }
    }

    render() {

        const CartItems = this.props.cart.items || [];

        return (
            <div ref="cartModal" id="cart-modal" className="cart-modal close" onClick={this.close}>
                <div className="inner" onClick={this.innerPrevent}>
                    <div className="overflow-content">
                        <div className="cart-topbar">
                            <div className="flex v-center h-sb">
                                <div className="col col-heading">
                                    <h3>Ваша корзина</h3>
                                </div>
                                <div className="col col-close">
                                    <a href="#close-cart" className="close-cart" onClick={this.close}>
                                        <span className="icon">
                                            <SvgSymbol class="svg" id="svg-icon-close" />
                                        </span>
                                        Закрыть
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="cart-header">
                            <div className="flex v-center h-sb">
                                <div className="col col-total-count">
                                    <span className="count number-caption">{CartItems.length}</span>
                                    <span className="text">METRO</span>
                                </div>
                                <div className="col col-total-price">
                                    <span className="price number-caption">
                                        <TotalPriceFormated />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="cart-subheder">
                            <div className="flex v-center h-sb">
                                <div className="col col-delivery">
                                    Ближайшая доставка: <span>сегодня, 22:00 - 00:00</span>
                                </div>
                                <div className="col col-info">
                                    <a href="#"><SvgSymbol class="svg" id="svg-icon-info" /></a>
                                </div>
                            </div>
                        </div>
                        <div className="cart-list">
                            {CartItems.map(product => <CartItem key={product.uuid} {...product} /> )}
                        </div>
                    </div>
                    <div className="cart-bottombar">
                        <a id="checkout-button" href="#" className="checkout-button button expanded">
                            сделать заказ
                            <span className="number-caption">
                                <TotalPriceFormated />
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {

    return { cart: state.cart };
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ toggleCart, updateCart }, dispatch)
}

export const localCart = () => {
    let cart;
    try { cart = JSON.parse(localStorage.getItem('svkaCart')); } 
    catch(err) {}

    return cart || { items: [] };
};

export const totalPrice = () => {
    const lc = localCart();
    return lc.items.reduce((d, c) => d + (parseInt(c.count) * parseFloat(c.price)), 0);
};

export const TotalPriceFormated = connect(mapStateToProps)(() => {
    return <Price key="cartTotalFormatedPrice" number={totalPrice()} />;
});


export const totalCount = () => {
    const lc = localCart();
    return lc.items.length;
};

export default connect(mapStateToProps, matchDispatchToProps)(Cart);