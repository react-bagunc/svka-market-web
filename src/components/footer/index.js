import React, { Component } from 'react';
import TopFooter from './top';
import MainFooter from './main';
import BottomFooter from './bottom';

class Footer extends Component {

    render() {
        return (
            <footer id="footer" className="footer box relative full-column full-border-top">
                <TopFooter />
                <MainFooter />
                <BottomFooter />
            </footer>
        );
    }

}

export default Footer;