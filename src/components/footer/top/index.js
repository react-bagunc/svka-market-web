import React, { Component } from 'react';
import SvgSymbol from './../../includes/svg/index';
import "./style.css";

class TopFooter extends Component {

    render() {
        return (
            <div className="top-footer flex v-center">
                <div className="col col-social">
                    <div className="content">
                        <h5 className="heading">Мы в социальных сетях</h5>
                        <ul className="menu flex">
                            <li><a href="https://fb.com" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-facebook" /></a></li>
                            <li><a href="https://vk.com" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-vk" /></a></li>
                            <li><a href="https://ok.ru" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-ok" /></a></li>
                            <li><a href="https://twitter.com" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-twitter" /></a></li>
                            <li><a href="https://google.com" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-google" /></a></li>
                            <li><a href="https://instagram.com" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-instagram" /></a></li>
                        </ul>
                    </div>
                </div>
                <div className="col col-subscribe">
                    <div className="content">
                        <h5 className="heading">Подписаться на рассылку</h5>
                        <form action="#" className="form subscribe-form">
                            <input type="email" placeholder="Ваша электронная почта" title="Ваша электронная почта" />
                            <button className="button small">Отправить</button>
                        </form>
                    </div>
                </div>
                <div className="col col-payment">
                    <div className="content">
                        <h5 className="heading">Способы оплаты</h5>
                        <ul className="menu flex">
                            <li><img src={process.env.PUBLIC_URL + "/assets/images/payment/4.png"} /></li>
                            <li><img src={process.env.PUBLIC_URL + "/assets/images/payment/3.png"} /></li>
                            <li><img src={process.env.PUBLIC_URL + "/assets/images/payment/2.png"} /></li>
                            <li><img src={process.env.PUBLIC_URL + "/assets/images/payment/1.png"} /></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopFooter;