import React, { Component } from 'react';
import "./style.css";

class BottomFooter extends Component {

    render() {
        return (
            <div className="bottom-footer">
                <div className="flex">
                    <div className="col col-copyright">
                        <span className="copyright">© 2019 Bitlate. Все права защищены</span>
                    </div>
                    <div className="col col-powered">
                        <span className="powered">
                            Разработано в <a href="https://sky-tech.org/" rel="noopener noreferrer" target="_blank">SkyTech</a>
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default BottomFooter;