import React, { Component } from 'react';
import SvgSymbol from './../../includes/svg/index';
import "./style.css";

class MainFooter extends Component {

    render() {
        return (
            <div className="main-footer bg-full light-silver">
                <div className="content">
                    <div className="flex">
                        <div className="col col-info">
                            <a href="index.html" className="logo">
                                <SvgSymbol class="icon" id="svg-icon-logo" />
                            </a>
                            <div className="phones">
                                <a href="tel:+88008008080">8 (999) 800-80-80</a>
                                <a href="tel:+88008008080">8 (880) 800-80-80</a>
                                <a href="#toggle-callback-modal">Заказать обратный звонок</a>
                            </div>
                            <div className="mails">
                                <a href="mailto:sale@bitlate.ru">sale@bitlate.ru</a>
                            </div>
                        </div>
                        <div className="col col-links">
                            <div className="flex h-center">
                                <div className="col">
                                    <ul className="links">
                                        <li>
                                            <a href="#">Каталог товаров</a>
                                            <ul className="sublinks">
                                                <li><a href="#">Авто и мототовары</a></li>
                                                <li><a href="#">Бытовая техника</a></li>
                                                <li><a href="#">Детям и мамам</a></li>
                                                <li><a href="#">Дом и сад</a></li>
                                                <li><a href="#">Зоотовары</a></li>
                                                <li><a href="#">Красота и здоровье</a></li>
                                                <li><a href="#">Одежда, обувь, акссесуары</a></li>
                                                <li><a href="#">Продукты питания</a></li>
                                                <li><a href="#">Ремонт и строительство</a></li>
                                                <li><a href="#">Спорт и отдых</a></li>
                                                <li><a href="#">Творчество и хобби</a></li>
                                                <li><a href="#">Электроника</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col">
                                    <ul className="links">
                                        <li>
                                            <a href="#">Компания</a>
                                            <ul className="sublinks">
                                                <li><a href="#">О компании</a></li>
                                                <li><a href="#">Новости</a></li>
                                                <li><a href="#">Акции</a></li>
                                                <li><a href="#">Контакты</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col">
                                    <ul className="links">
                                        <li>
                                            <a href="#">Интернет-магазин</a>
                                            <ul className="sublinks">
                                                <li><a href="#">Оплата заказа</a></li>
                                                <li><a href="#">Доставка товара</a></li>
                                                <li><a href="#">Возврат и обмен</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col">
                                    <ul className="links">
                                        <li>
                                            <a href="#">Информация</a>
                                            <ul className="sublinks">
                                                <li><a href="#">Поставщикам</a></li>
                                                <li><a href="#">Гарантия</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainFooter;