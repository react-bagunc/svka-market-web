import React, { Component } from 'react';
import './style.css';

class TabSwitcher extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            tab: 0
        };
    }

    navigation(event, tab) {
        event.preventDefault();

        this.setState({ tab })
    }   

    tabs() {
        
        return (
            <div className="tabs">
                <ul>
                    {this.props.options.map((item, idx) => this.tab(item, idx))}
                </ul>
            </div>
        );
    }

    tab(item, idx) {

        let cls = [];
        if (this.state.tab === idx) cls.push("current");
        if (item.class) cls.push(item.class);

        return (
            <li key={`tab-${idx}`} className={cls.join(' ')}>
                <a href="#" onClick={(e) => this.navigation(e, idx)}>{item.tab}</a>
            </li>
        );
    }

    contents() {
        
        return (
            <div className="tab-contents">
                {this.props.options.map((item, idx) => this.content(item, idx))}
            </div>
        );
    }

    content(item, idx) {
        let cls = ["tab-content"];
        if (this.state.tab === idx) cls.push("current");
        if (item.class) cls.push(item.class);

        return (
            <div key={`tab-content-${idx}`} className={cls.join(' ')}>{item.content}</div>
        );
    }


    render() {

        return(
            <div className="tab-switcher ready">
				{this.tabs()}
				{this.contents()}
			</div>
        );
    }

    
}

export default TabSwitcher;