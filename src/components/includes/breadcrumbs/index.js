import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './style.css';
import { connect } from 'react-redux';

class Breadcrumbs extends Component {

    render() {
        let links = Object.keys(this.props.catalog.response.breadcrumbs || {}),
            breadcrumbs = [],
            heading = this.props.catalog.response.description || "";

        if (heading)
            heading = <h1 className="section-heading">{heading}</h1>;

        if (links.length) {
            links.forEach((text, idx) => {
                let link = links.length - 1 === idx ? 
                    <span key={this.props.catalog.response.breadcrumbs[text]}>{text}</span> : 
                        <Link key={this.props.catalog.response.breadcrumbs[text]} to={`/catalog/${this.props.catalog.response.breadcrumbs[text]}/`}>{text}</Link> 
                breadcrumbs.push(link);
            })
        }
        
        return (
            <section id="page-info-section" className={`section section-page-info bg-full no-padding ${this.props.classes}`}>
                <div className="content">
                    {links.length ?
                    <div className="breadcrumbs">
                        <nav className="menu">
                            <ul className="inline">
                                <li><Link to="/">Главная</Link></li>
                                {breadcrumbs}
                            </ul>
                        </nav>
                    </div>
                    : ""}
                    {heading}
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {

    return { catalog: state.catalog };
}

export default connect(mapStateToProps)(Breadcrumbs);