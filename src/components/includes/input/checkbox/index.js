import React from 'react';
import './style.css';

function InputCheckbox(props) {
    return (
        <label className="choose">
            <input {...props} hidden={true} />
            <span className="elem"></span>
            <span className="text">{props.text || ""}</span>
        </label>
    );
}

export default InputCheckbox;
