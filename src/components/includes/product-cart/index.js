import React, { Component } from 'react';
import SvgSymbol from '../svg';
import Price from '../price';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { addToCart, removeCartItem } from '../../../actions/cart';
import InputCounter from './../input-counter/index';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './style.css';

class ProductCart extends Component {

	constructor(props) {
		super(props);

		this.state = {
			galleryIndex: 0,
			count: 1
		};

		this.selectGalleryItem = this.selectGalleryItem.bind(this);
		this.icAfterUpdate = this.icAfterUpdate.bind(this);
	}

	selectGalleryItem(event, galleryIndex) {
		event.preventDefault();

		this.setState({ galleryIndex })
	}

	icAfterUpdate(ic) {        
        if (ic.value > 0) {
            this.props.addToCart(Object.assign(
                {...this.props}, 
                { count: ic.value }
            ));
        } else {
            this.props.removeCartItem({ uuid: this.props.uuid });
        }
        this.setState({ count: ic.value > 0 ? ic.value : 1 });
    }

    render() {

		let article = "";
		if (this.props.characteristics)
			if (this.props.characteristics.article)
				article = <span className="code">Артикул: {this.props.characteristics.article}</span>

		return (
            <div className="grid product">
				<div className="box gallery-box">
					<div className="labels-block"></div>
					{this.props.images ? 
					<LazyLoadImage 
						src={this.props.images[this.state.galleryIndex] || process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
						effect="black-and-white"
						wrapperClassName="thumbnail"
						placeholderSrc={process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
						/> : ""}
					<div className="zoom">
						<a href="#">
                            <SvgSymbol class="icon" id="svg-icon-search" />
							Увеличить
						</a>
					</div>
					<div className="gallery">
						{this.props.preview ?
						this.props.preview.map((img, idx) =>
							<a 	key={`${this.props.uuid}-gallery-${idx}`}
								href={img} 
								onClick={(e) => this.selectGalleryItem(e, idx)} 
								className={this.state.galleryIndex === idx ? 'current': ''}>
								<LazyLoadImage 
									src={img || process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
									effect="black-and-white"
									placeholderSrc={process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
									/>
							</a>
						) : ""}
					</div>
				</div>
				<div className="box info-box">
					<h1 className="section-heading">{this.props.name}</h1>
					{article}
				</div>
				<div className="box actions-box">
					<div className="flex v-center">
						<div className="col col-price">
							<span className="price"><Price number={(this.props.price * this.state.count)} /></span>
						</div>
						<div className="col col-add-to-cart-button">
							<InputCounter afterUpdate={this.icAfterUpdate} />
						</div>
						<div className="col">
							<button className="button secondary">
								<span>Купить в 1 клик</span>
							</button>
						</div>
					</div>
					<div className="flex links">
						<div className="col"></div>
						<div className="col">
							<a href="#like" className="like">
                                <SvgSymbol class="icon" id="svg-icon-liked" />
								<span>В избранное</span>
							</a>
						</div>
						<div className="col">
							<a href="#compare" className="compare">
                                <SvgSymbol class="icon" id="svg-icon-compare" />
								<span>Сравнить</span>
							</a>
						</div>
					</div>
				</div>
				<div className="box additional-box">
					<div className="description">{this.props.description}</div>
					<div className="social-block">
						<a href="https://fb.com/" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-facebook" /></a>
						<a href="https://vk.com/" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-vk" /></a>
						<a href="https://ok.ru/" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-ok" /></a>
						<a href="https://twitter.com/" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-twitter" /></a>
						<a href="https://google.com/" rel="noopener noreferrer" target="_blank"><SvgSymbol class="icon" id="svg-icon-social-google" /></a>
					</div>
				</div>						
			</div>
        );
    }

}

function matchDispatchToProps(dispatch) {

	return bindActionCreators({ addToCart, removeCartItem }, dispatch);
}

export default connect(null, matchDispatchToProps)(ProductCart);