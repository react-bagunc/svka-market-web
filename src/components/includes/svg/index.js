import React from 'react';
import Icons from './icons.svg';

const SvgSymbol = props => (
    <svg className={`${props.class || ""}`}>
      <use xlinkHref={!props.fromDefs ? `${Icons}#${props.id}` : `#${props.id}`} />
    </svg>
  );

export default SvgSymbol;