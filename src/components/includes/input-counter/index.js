import React, { Component } from 'react';
import SvgSymbol from './../svg/index';
import { isFunction } from 'util';

import './style.css';

class InputCounter extends Component {

    constructor(props) {
        super(props);

        this.wrapper = props.wrapper || null;
        this.value = this.props.value || 0;
        this.min = this.props.min || 0;
        this.max = this.props.max || Infinity;
        this.step = this.props.step || 1;

        this.disableAnimation = this.props.disableAnimation || false

        this.state = {
            value: this.value,
            open: false
        }

        this.decrement = this.decrement.bind(this);
        this.increment = this.increment.bind(this);
    }

    update(options) {
        if (isFunction(this.props.beforeUpdate))
            this.props.beforeUpdate(options);

        this.setState(options);

        if (isFunction(this.props.afterUpdate))
            this.props.afterUpdate(options);
    }

    decrement(event) {
        event.preventDefault();

        let options = { open: true };
        if (--this.value <= this.min) {
            this.value = this.min;
            options.open = false;
        }
        options.value = this.value;

        this.update(options);
    }

    increment(event) {
        event.preventDefault();

        let options = {}
        if (++this.value > this.max) {
            this.value = this.max;
        }
        options.open = true;
        options.value = this.value;

        this.update(options);
    }

    render() {

        let wrappClass = ["input-counter"];
        if (this.state.open && !this.disableAnimation)
            wrappClass.push("open");

        return (
            <div className={wrappClass.join(' ')}>
                <a className="decrement" href="#decrement" onClick={this.decrement}>
                    <SvgSymbol class="svg" id="svg-icon-minus" />
                </a>
                <input  type="number" 
                        value={this.value} 
                        min={this.min} 
                        max={this.max}
                        step={this.step}
                        readOnly={true}
                    />
                <a className="increment" href="#increment" onClick={this.increment}>
                    <SvgSymbol class="svg" id="svg-icon-plus" />
                </a>
            </div>
        );
    }

}

export default InputCounter;