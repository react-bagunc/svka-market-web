import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import InputCounter from './../input-counter/index';
import Price from '../../includes/price';
import { addToCart, removeCartItem } from './../../../actions/cart';

import './style.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { is_mobile_mode } from '../../../dll';
import { setQuickViewQuery } from './../../../actions/quick-view';
import { window_scroll } from './../../../dll';

class ProductItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            count: 1 
        };

        this.icAfterUpdate = this.icAfterUpdate.bind(this);
        this.openProductPage = this.openProductPage.bind(this);
    }
    

    openProductPage(event) {

        if (!is_mobile_mode()) {
            event.preventDefault();
            window_scroll(false);
            this.props.setQuickViewQuery({
                uuid: this.props.uuid,
            });
        }

    }

    icAfterUpdate(ic) {
        let wrap;

        if (wrap = this.refs[this.props.uuid]) {
            wrap.classList[ic.open === true ? "add" : "remove"]("counter-active");
        }
        
        if (ic.value > 0) {
            this.props.addToCart(Object.assign(
                {...this.props}, 
                { count: ic.value }
            ));
        } else {
            this.props.removeCartItem({ uuid: this.props.uuid });
        }
        this.setState({ count: ic.value > 0 ? ic.value : 1 });
    }

    render() {

        let thumbnailWrapperClasses = ["thumbnail"];
        
        if (!this.props.preview)
            thumbnailWrapperClasses.push("no-image");

        return (
            <div ref={this.props.uuid} className="product">
                <div className="content">
                    <div className="labels-block"></div>
                    <LazyLoadImage 
                        src={this.props.preview || process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
                        effect="black-and-white"
                        wrapperClassName={thumbnailWrapperClasses.join(' ')}
                        placeholderSrc={process.env.PUBLIC_URL + '/assets/images/placeholder.png'}
                        />
                    <h3 className="name">
                        <Link to={`/product/${this.props.uuid}/`} onClick={this.openProductPage}>{this.props.description}</Link>
                    </h3>
                    <div className="price-block">
                        <Price number={(this.props.price * this.state.count)} />
                    </div>
                    <div className="additional-block">
                        <InputCounter   wrapper={this.refs[this.props.uuid]}
                                        afterUpdate={this.icAfterUpdate}
                                        disableAnimation={true} />
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ addToCart, removeCartItem, setQuickViewQuery }, dispatch)
}

export default connect(null, matchDispatchToProps)(ProductItem);