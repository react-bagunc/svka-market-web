import React from 'react';

export default (props) => {
    
    return (
        <span className="price">{
            new Intl.NumberFormat('ru-RU', {
                minimumFractionDigits: props.minimumFractionDigits || 2
            }).format(props.number || 0)
        } <span className="currency rub">{props.currency || "Р"}</span></span>
    );
};