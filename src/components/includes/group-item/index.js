import React, { Component } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { setCatalogQuery } from './../../../actions/catalog';
import { connect } from 'react-redux';


class GroupItem extends Component {

    constructor(props) {
        super(props);

        this.choose = this.choose.bind(this);
    }

    choose() {
        this.props.setCatalogQuery({
            parent: this.uuid,
            numpage: 1,
            attributes: []
        });
    }

    render() {
        return (
            <div className="box">
                <Link to={`/catalog/${this.props.uuid}/`} onClick={this.choose}>
                    <div className="image">
                        <LazyLoadImage 
                            src={this.props.image}
                            effect="black-and-white"
                            />
                    </div>
                    <h3 className="heading">{this.props.description}</h3>
                </Link>
            </div>
        );
    }
}

function matchDispatchToProps(dispatch) {

    return bindActionCreators({ setCatalogQuery }, dispatch)
}

export default connect(null, matchDispatchToProps)(GroupItem);