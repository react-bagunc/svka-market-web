import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPage } from './actions/getpage';

import SvgDefs from './components/includes/svg/defs';
import TopHeader from './components/header/top/index';
import MainHeader from './components/header/main/index';
import Footer from './components/footer/index';
import MobileMenu from './components/mobile-meu';
import Content from './components/content/index';
import Cart from './components/cart';
import Auth from './components/auth/index';
import ProductQuickView from './components/quick-view/index';

class AppRouter extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        fetch('//1c.uka74.ru/apisk/hs/site/getpage', { method: 'post' })
        .then(response => response.json())
          .then(response => {
            this.props.getPage(response);
          });
    }

    render() {
        return (
            <Router>
                <SvgDefs />
                <MobileMenu response={this.props.payload.menu} />
                <main id="wrapper" className="wrapper grid h-center">
                    <div className="container">
                        <TopHeader />
                        <MainHeader />
                        <Content response={this.props.payload} />
                        <Footer />
                    </div>
                </main>
                <Cart />
                <Auth />
                <ProductQuickView />
            </Router>
        );
    }
}

function mapStateToProps(state) {

    return { payload: state.getpage }
}

function matchDispatchToprops(dispatch) {

    return bindActionCreators({ getPage }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToprops)(AppRouter);