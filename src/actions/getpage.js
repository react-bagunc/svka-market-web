export const getPage = (payload) => {

    return {
        type: "GET_PAGE",
        payload
    };
};