export const setProduct = (payload) => {

    return {
        type: "SET_PRODUCT",
        payload
    };
};

export const setProductQuery = (payload) => {

    return {
        type: "SET_PRODUCT_QUERY",
        payload
    };
};

export const setProductResponse = (payload) => {

    return {
        type: "SET_PRODUCT_RESPONSE",
        payload
    };
};

export const setProductFetch = (payload) => {

    return {
        type: "SET_PRODUCT_FETCH",
        payload
    };
};