export const toggleAuth = (payload) => {

    return {
        type: "TOGGLE_AUTH",
        payload
    }
}

export const switchAuth = (payload) => {

    return {
        type: "SWITCH_AUTH",
        payload
    }
}