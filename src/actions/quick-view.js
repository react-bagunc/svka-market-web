export const toggleQuickView = (payload) => {

    return {
        type: "TOGGLE_QUICK_VIEW",
        payload
    };
}

export const setQuickView = (payload) => {

    return {
        type: "SET_QUICK_VIEW",
        payload
    };
}

export const setQuickViewQuery = (payload) => {

    return {
        type: "SET_QUICK_VIEW_QUERY",
        payload
    };
}

export const setQuickViewResponse = (payload) => {

    return {
        type: "SET_QUICK_VIEW_RESPONSE",
        payload
    };
}

export const setQuickViewFetch = (payload) => {

    return {
        type: "SET_QUICK_VIEW_FETCH",
        payload
    };
}