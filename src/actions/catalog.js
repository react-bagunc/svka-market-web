export const setCatalogParent = (payload) => {
    return {
        type: "SET_CATALOG_PARENT",
        payload
    };
};

export const setCatalogQuery = (payload) => {
    return {
        type: "SET_CATALOG_QUERY",
        payload
    }
}

export const setCatalogResponse = (payload) => {
    return {
        type: "SET_CATALOG_RESPONSE",
        payload
    }
}

export const setCatalogFetch = (payload) => {
    return {
        type: "SET_CATALOG_FETCH",
        payload
    }
}

export const setCatalogLoadMore = (payload) => {
    return {
        type: "SET_CATALOG_LOADMORE",
        payload
    }
}

export const setCatalogOrder = (payload) => {
    return {
        type: "SET_CATALOG_ORDER",
        payload
    }
}

export const setCatalogFilter = (payload) => {
    return {
        type: "SET_CATALOG_Filter",
        payload
    }
}

export const toggleCatalogMenu = (payload) => {
    return {
        type: "TOGGLE_CATALOG_MENU",
        payload
    }
}

export const toggleCatalogFilter = (payload) => {
    return {
        type: "TOGGLE_CATALOG_FILTER",
        payload
    }
}