export const toggleCart = (payload) => {

    return {
        type: "TOGGLE_CART",
        payload
    };
};

export const addToCart = (payload) => {

    return {
        type: "ADD_TO_CART",
        payload
    };
};

export const removeCartItem = (payload) => {

    return {
        type: "REMOVE_CART_ITEM",
        payload
    }
};

export const updateCart = (payload) => {

    return {
        type: "UPDATE_CART",
        payload
    }
};