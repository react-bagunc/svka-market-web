import queryString from 'query-string';

export const window_scroll = (enable = true) => {
    let method = "add";
    if (enable === "toggle") method = "toggle";
    if (enable === true) method = "remove";
    document.querySelector('html').classList[method]('no-scroll');
};

export const is_mobile_mode = () => {
    return window.innerWidth < 998;
};

export const $_get = (key) => {
    let search = queryString.parse(window.location.search) || {};
    return search[key];
};

export const phoneToString= (number) => {

    return number.replace(/ |\(|\)|\-|\+/g, '');
}

export const detectBrowser = () => {
    (function(){
        // var browsers = {};
        // browsers.opera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // browsers.firefox = typeof InstallTrigger !== 'undefined';
        // browsers.safari = /constructor/i.test(window.HTMLElement) || 
        // (function (p) {  return p.toString() === "[object SafariRemoteNotification]"; })
        // (!window['safari'] || safari.pushNotification);
        // browsers.ie = false || !!document.documentMode;
        // browsers.edge = !browsers.ie && !!window.StyleMedia;
        // browsers.chrome = !!window.chrome && !!window.chrome.webstore;
        // browsers.blink = (browsers.chrome || browsers.opera) && !!window.CSS;
        var bname = "chrome";
        // for(var name in browsers){
        //     if(browsers[name]===true){
        //         bname = name;
        //             break;
        //     }
        // }
        return document.body.classList.add('browser-'+bname);
    })();
};