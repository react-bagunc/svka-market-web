import Page404 from './../components/content/404/index';
import HomePage from './../components/content/home/index';
import CatalogPage from './../containers/catalog';

const pagesMap = {
    HomePage,
    CatalogPage,
    Page404
};

export default pagesMap;